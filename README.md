# dotnet.awscliv2.docker

This project builds a docker image with basic tools for .NET projects based on Docker and Amazon.

## Usage
Ex. Inside your .gitlab-ci.yml file make the following changes:
```
image: registry.gitlab.com/itomychstudio/builders/awscliv2.sam.docker.dotnet5:latest

stages:
  - build

variables:
  SAM_CLI_TELEMETRY: 0
  DOTNET_CLI_TELEMETRY_OPTOUT: 1
  DOTNET_SKIP_FIRST_TIME_EXPERIENCE: 1

build:
  stage: build
  services:
    - docker:20.10.8-dind
  before_script:
    - docker -v
    - dotnet --version
    - aws --version
    - sam --version
  script:
    - echo "Hello World!"
```

## Tools
  - aws-cli v2
  - aws sam cli
  - dotnet sdk 5.0.401
  - docker v20.10.7
  - docker-compose v1.28 
  - bash
  - yamllint

## Contributing
Create branch with the 'feature/ticket-name' name. Make changes and commit.
Then create the merge request into 'develop' branch.

***
## Authors and acknowledgment
ITOMYCH STUDIO/DashDevs LLC
